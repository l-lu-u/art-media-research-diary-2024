---
title: "papers, books, tools, and everything else"
date: 2024-01-08T09:39:28-07:00
---

## About setting up this site: some KaTex and Hugo
- <https://www.endpointdev.com/blog/2023/02/how-to-create-a-hugo-website-without-a-theme/>

## Jonna's reading list
During the past few months I've been to many bookstores, here's some cool stuff I found:
- The Rainbow Book, 1979 < a collection of everything about rainbows, really cool graphic design
- Commons in Design, 2023 <https://valiz.nl/en/publications/commons-in-design>
- Cyber Feminisim Index, <https://cyberfeminismindex.com/>
- Objective: Earth, Designing our Planet, <https://mudac.ch/en/objective-earth/>
- CAPS LOCK, Ruben Pater, <https://valiz.nl/en/publications/caps-lock>
- What is post-branding?, Jason Grant & Oliver Vodeb, <https://www.setmargins.press/books/what-is-post-branding/>
- Most touched, <https://form.de/en/products/most-touched>
- Fancy Creatures, <https://www.konomad.com/page/shop/book/book_fancycreatures.html>
- Skin and Code, Daniel Neugebauer + others, <https://spectorbooks.com/book/skin-and-code>