---
title: "Week 13: Molding & Casting Group Assignment"
date: 2024-04-23T09:39:28-07:00
---


- mold star (15 slow) platinum silicone rubber
- Sorta clean (the food safe option)
- Smooth cast

1. Wear gloves, protective glasses and a lab coat, since the process can get messy verz quickly 
2. Mix mold start a + b (35~40gram each) - calculate the needed amount based on the volume and density of the mold star liquid
3. Read its datasheet 
4. Shrinkage over time (not considered now)
5. Mix mix Stir stir until it gets more fluid (for both) and then pour and measure
6. Better to level to the top of the mold

[![Detail image](./images/silicone_1.jpg)](./images/silicone_1.jpg)

CLEAN SURFACES:
Try to have a few paper sheets by your side at all times, as the process of silicone mixing (and especially pigment mixing) can be very messy. If the silicone mixture (or the mold) gets contaminated with dust or other particles, it might mess up the silicone's ability to set.

STICKS:
You should be brutal about the sticks. If you accidentally mix a bit of A into the bottle of B you will start the reaction for the whole bottle, messing up a lot of fairly expensive material. So, rather be a bit more wasteful in terms of the sticks (horrible to say), than waste bottle of silicone. If you clean them thoroughly with a paper sheet after every use (before the cure time begins and the silicone starts to thicken), you can reuse them as many times as you want.

GLOVES:
Similar idea for the gloves. Better change them more often, to avoid contamination and to not cover every imaginable surface in silicone.


[![Detail image](./images/silicone_3.jpg)](./images/silicone_3.jpg)

MIXING:
The liquid silicone is typically composed of two parts: a base material and a curing agent. The ratio for the silicone mixing is usually 1:1, but it's still important to check the datasheet, just to be sure. Mix by rotating the stick around, not by pulling up and down (this introduces more bubbles). Stir it gently in circular motions, making sure to reach the bottom of the container where a layer has usually started to solidify. Be patient and imagine how it must have felt to beat butter manually, or extract honey out of honeycombs manually. We have it easy.

[![Detail image](./images/silicone_2.jpg)](./images/silicone_2.jpg)

THE CURING PROCESS:
One of the important things to check on the datasheet is the pot life and cure time. Pot life is the time window in which you can apply and manipulate the silicone before it begins to cure. Sometimes it can be as short as 5 minutes, which means you might have to degass and pour the silicone mix in this timeframe! Cure time is the duration required for the silicone to fully cure or solidify into its final form. It can range from a few minutes to several hours.. There are some conditions that can affect the curing time, such as humidity and temperature. The datasheet will sometimes offer information on how to speed up the curing time (for example by putting it ino a hot box or industrial oven). You can also apply a release agent, as the demolding process with some molds (especially bigger ones) can become very challenging. It's also important to check the datasheet for any material that could prevent the silicone from curing (in our case sulfur, so not so important).


[![Detail image](./images/silicone_8.jpg)](./images/silicone_8.jpg)

DEGASSING:
Advisable, not always necessary. Check the datasheet to see how necessary it is, since it depends based on the silicone type. Can be done for the mixture in the cup or for mixture in the mold. If using the mold star, fairly liquidy silicone, it is more likely to 'explode' in the mold than in the cup - the pressure difference pulls the liquid, and since the mold is filled to the brim it is more likely to leak over. The degassing procedure goes like this:
1. Put your cup or mold into the chamber
2. Put the lid securely on.
3. Make sure the plug is in place
4. Turn on the degasser
5. Watch the pressure arrow go up to 400

[![Detail image](./images/silicone_9.jpg)](./images/silicone_9.jpg)

6. Pull out the plug and so pull out some of the air
7. Repeat for several times (depends on the amount of air present)

If using the sorta-clear, food-safe silicone, it has an intense amount of bubbles after mixed, plus it is extremely viscous in comparison to the non food-safe silicons. The air does not really want to escape when in the degassing chamber, but some of the bubbles naturally escape. Plus the air bubbles go up, so they are less likely to get stuck at the bottom, next to the actual features.

For smooth cast
1. Calculate the gram (10 A : 9 B)
2. Part A & B are both quite liquid-ish compared to mold start

100A + 90B = 3 g (that weight of the thing)

BUT! How will we know???????????? >>> actually the  important thing here should be the ratio, the amount is better to be slightly more than the actual weight of the final outcome (can be estimated or calcuated)

The casting process (silicone mold):

1. Mixing the parts separately.
Each of the two parts (A and B) need to be thoroughly mixed before poured out into the mixing cup. How long this will take depends on how long it has been sitting – if someone mixed them yesterday, you are lucky. USE SEPARATE STICKS AND DO NOT CONTAMINATE.

[![Detail image](./images/silicone_4.jpg)](./images/silicone_4.jpg)

2. Weighing the parts
Put the cup on to the scales and zero the weight to the cup weight. How much of each part is needed depends on the ratio given in the data sheet and the the total amount of material you need for the mold. If measuring by volume (which we don't do), you could get the total volume of your mold either from the 3D software or from pouring water into your mold till it is full, pouring it out into a cup, then weighing that. This gives you the volume since 1ml of water = 1 g of water. If you know the density of your silicone you can then calculate the needed amount. Or you use your intuition and waste material (risk is fun).
The datasheet gives either weight or volume ratios, so important to pay attention to which one is being given and whether the two parts are of equal density (so you can transfer the ratio directly).

[![Detail image](./images/silicone_5.jpg)](./images/silicone_5.jpg)

3. Mixing the two parts (USE A THIRD STICK).
Mix them thoroughly - really thoroughly.

[![Detail image](./images/silicone_6.jpg)](./images/silicone_6.jpg)

4. Degass now or after the mixture is poured into the mold.
5. The mold should be cleaned so that there is no residue on the botton - this will be cast into your silicone.
6. Pour from one corner, in as small of a stream as you can manage (thickness of a guitar string). It's important to keep the pour in one place. If you need to move the cup to another place, do it slowly. Tap the mold on the table to remove any stuck air bubbles. Be careful to not pour over the mold.

[![Detail image](./images/silicone_7.jpg)](./images/silicone_7.jpg)

7. Leave the mold to set for 4 hours. Don't be silly and leave it completely horizontal if you don't want future problems.

The casting process (liquid plastic) - very similar 1-5, and then:
5. If you are doing a double-sided mold, you need to join the two parts at a particular point in the setting process. Essentially, when it is partially set, but not so set as to not join together. 7 minutes into the process? Check the time it takes to pre-set on the doc sheet - needs to be less than that. When combining, make sure the pins meet correctly, be agile, be nimble, get it right. Then let it sit happily - can add a weight on top to seal it happily.

[![Detail image](./images/silicone_10.jpg)](./images/silicone_10.jpg)

Take out your beautiful something!
