---
title: "2024.01.08 - New Media Programme Introduction"
date: 2024-01-08T09:39:28-07:00
---

**Jonna:**
What do I expect? I'm excited to learn about different types of research that fall under arts and media. My current knowledge is very design focused.

**Lù:** Same here! I am very curious about how to conduct artistic research - there are methods and tools also adopted in design research, but what makes the inquiry into arts differ from those into design studies even though both are stemming from creative practices? I WONDER...!

Another interesting point is, when talking about the learning diary assignment, Lily suggested us to think about the "categories" for the diary and how the categorisation can benefit for our own practices. It makes me contemplate about journaling, in some way, this documentation can be the initial step of archiving my own practices. Also, more than just the records of what I have done and learnt, it notes down ideas for the future as well.