---
title: "2024.01.22 - Joint Lecture 2: What is research?"
date: 2024-01-22T10:39:28-07:00
---

## Lecture: "What is research" by Max Ryynänen​

**Lù:** Example of Natyasastra >> Philosophy is not an European thing *[<< yet, I think probably only Eurocentric minds would think this way :/]*
- Bharata Muni (300BC - 300AD), descriptive
- Abhinavagupta (950 - 1016), analytic
- from the examples above >> Root books & Classics

**Jonna:** In Finnish schools philosophy is more or less taughed as if it was only invented in ancient Greece and then everyone else copied them. Though in general, philosophy isn't talked much at all. There was only one mandatory philosophy course in high school and that's it.

**Lù:** Film studies "similar to science studies" >> semiotics >> in film studies, the unit of analysis such as cuttings and images

**Lù:** Franz Kafka, Investigations of a Dog (1922)
- "The dog is sort of doing research"

**Lù:** Ajnãna -> Quite well-developed scepticism already 700BC
- If knowledge are prime (what?)
- "China" as a metaphor of (partial) fact, "People do go to China" *[which is a slighly quirky example for me]*
- Ancienct, radical Indian scepticism
    - Knowledge is relative
    - Hindu-, Jain-
- Knowledge at all maybe not possible ("natural science is also changing every other day")
- ^ "fact" >> "cultural fact" >> a "fact" that may has a few modification <- we don't need a "total fact" (to form knowledge)

**Lù:** Scepticism as method
- Al-Ghazzali (1057-1111)
- Teresa de Avila (1515-1582, Spain) << a painting from François Gérard's painting 1827

**Lù:** Rationalism
- Deduction
- Renè Descartes (1596-1630, portrait by Frans hals)
- 1637 discourse on the METHOD
- Method of DOUBTING and scepticism
- Everyday scepticism >> watching, e.g. houses

**Lù:** Empiricism
- Quantitative / Qualitative (very vague description here)
- Knowledge and/or justification comes from sensory experience
- No innate ideas (Locke: tabula rasa)
- Empiric Greek philosophers - 300 BC (Gorgias, Hippas vs. Plato even Aristotle)
- Bacon, Berkeley, Hume (1600-1750)

**Lù:** Phenomenology 

**Lù:** The role of **writing** in knowledge generation

**Lù:** What is knowledge? How to present research results?
- Existence
- Open for critique
- Different traditions / ways
- Epistemology (how to get knowledge) >> Sources: perception, memory, testimony, reason
    - Truth and justification (conceptional definition)
    - Skepticism
    - Scientific community
        - Epistemologies >> De Sousa Santos
- Language formation >> how the structure of language influence the knowledge generation (e.g., subject-object relations)

**Lù:** Orders of knowledge
- Disciplines - Methods - Authorities - Testing truth-value

**Lù:** Research methods for art and design
- Human-centred design methodologies (e.g., webpages, study of collaboration, empathy, experimentation << but not only the end result, also the process)
- Background research for exhibitions
- Exhibition as research
- Design as research (experimentality)

**Lù:** Discourse analysis
- Hel Cault
- Leo Spitzer >> Michel Foucault
- The Archeology of Knowledge (1969): meanings produced by language use and communication - production, change and negotiations of meaning (from the point of view of power and empowerment)

**Jonna:** Writing with the body (Raimun Hodge, RIP 2021)
- Researching body movements through dance, what could new media add to this?
- Embodied interaction with machine. Communicating with machines through full body movements. What would the interaction feel?

**Jonna:** Quantitative methods don't always produce "hard" knowledge. Sometimes, qualitative provides much more accurate and valuable data. 