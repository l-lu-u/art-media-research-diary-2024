---
title: "2024.03.04 - New Media Session 3: Ethnography, Fieldwork, Autoethnography"
date: 2024-03-04T10:39:28-07:00
---

*About ethnography and autoethnography*

## Lecture

**Lù:** Vygotsky "Mind in society" - a linguist and language translator & psychology -> experiment making ordinarily hidden behind "habitual behavior" visible processes -> but fieldwork is not like this

**Lù:** "Fieldwork" one of the most important aspects of ethnographic work -> originated in *human sciences* during the 18th and 19th centuries, e.g., travel literature (e.g., Helinä Rautavaara's collection -> a major ethnography museum in Finland)

**Lù:** "Fieldwork" as a research method
    - can provide detailed and intimate understanding of *social action* in relation to its/their context
        - allow an outsider to gain insider's perspective
        - allow the insider to look at everyday reality in a new way
    - can occur in diverse settings not just in *travellers' adventures*
        - an archive is not a libray, it preserves the situations happened
    - **Ethnography** is used part of fieldwork -> "participant observation" is a term coined by Bronislav Malinowsky
        - the anthropologist aims to "become a part of the group under study"
        - Clifford Geertz: the anthropologist aims to enlarge human discourse and understanding of others' "normalness ithout reducing their particularity" -> a form of *translation* or *interpretation* -> ethnographic approach concerned with "thich descriptions"
        - "all cultural translations, necessarily involve an element of transformation or even disfiguration"
    - **How do we set about decoding people's customs?** -> 3 focuses raised by Leach, E. (1976). *Culture and Communication: The Logic by which Symbols Are Connected,* Cambridge University Press
        - natural biological activities (e.g., breathing?)
        - technical actions (e.g., the activities by human to alter their environment)
        - expressive relations (e.g., the communications occured between secondary and tertiary artifacts) <- what are these?
    - A line drawing of Hammock by Edmund Leach + **a kinship system diargam**
    - POV: the positionality of the researcher *("Who am I? in relation to the community I am researching?")*
        - an *emic* perspective: insider
        - an *etic* viepoint: outsider
        - reflexive approach -> no researcher can ever be one hundred percent objecgive
    - **Autoethnography** -> personal histories in the form of reconstructions of life
        - hindsight
        - **"Experience is something that is consitituted socially through discourse and performance"**
        - “Person” is a cultural creation
    - **Epiphanies**
        - interactional moments perceived to have significance and impact in a person's life (e.g., "coming-out-partyyy"?)
        - Denzin, N.K. (2014). *Interpretive Autoethnography, Qualitative Research Methods*, Vol.17, Los Angeles: Sage Publications Inc.
        - Denzin describes 4 types of epiphanies
            - major event
            - cumulative or representative 
            - minor
            - relieved
    - In sum -> "ethnographic and autoethnographic narratives are great source material for storytelling"
    - **!!! Note** As researchers, how can we
        - promote clarity
        - untangle complexity
        - reveal implicit structure
        - gathering of total views -> as they unfold in time <- for a MA thesis, how can we work with the limited timeframe -> **limit the context & scope**
            - 1 hour fieldwork -> how much time to process it?
            - R language for content analysis <- R or Python, or (tools like Atlas)?

**Jiayi:** 

**Objectives**
- To distinguish between emic and etic perspectives in ethnographic studies.
- To explore the concept of epiphanies in autoethnographic research.

**Key Learnings**
- **Ethnography** is a fundamental method in anthropology used during fieldwork to gain a deep, contextual understanding of social actions.
- **Clifford Geertz's View**: Ethnography aims to expand human discourse and enhance understanding of different cultural norms while acknowledging their uniqueness.
- **Emic and Etic Perspectives**: The emic perspective focuses on the insider's view, while the etic perspective considers an outsider’s objective viewpoint.
- **Autoethnography** involves reflecting on past experiences and using various artifacts like interviews, texts, and photographs to analyze these experiences.

**Insights and Reflections**
- Ethnography allows an outsider to feel like an insider, bridging gaps in cultural understanding and communication.
- The distinction between emic and etic perspectives is crucial for interpreting cultural phenomena accurately without bias.
- Epiphanies in autoethnography can significantly alter one’s understanding and perception of life events, highlighting the profound impact of cultural identity and interactions.

**Questions and Curiosities**
- How can one effectively balance emic and etic perspectives in a single ethnographic study without compromising the depth of either viewpoint?
- What are the practical challenges of conducting autoethnography, especially concerning bias and subjective interpretations?
- How might the understanding of epiphanies change if applied in a non-cultural or non-personal context?

**Possible Resources to Look Into Later**
- **"Interpretive Autoethnography" by Danzin**: For a deeper dive into different types of epiphanies and their implications in qualitative research.
- **Clifford Geertz’s Works**: More on his anthropological methods and theories.
- Case studies or journals on media archaeology as it relates to ethnography.