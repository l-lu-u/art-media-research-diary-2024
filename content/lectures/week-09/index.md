---
title: "2024.03.11 - New Media Session 4: Artefact Analysis"
date: 2024-03-11T10:09:28-07:00
---

## Lecture: Artefact Analysis

**Lù:** Artefacts 
- "artefacts" -> artefactum (?) -> when designing one, is to envision the context of use -> "post-digital", what does it mean?
- characteristics - risto hilpinen (philosopher)
    - self-referential
    - accurate
    - scientific
    - ontological: singular, concrete, non-seperate object -> replicas, "a scene of hyperreality" (Umberto Eco) -> "Hyper-reality is more real than the real" -> just like the writings on Disneyland
    - authorship
- Susan Pearce "Thinking about things"
- Fluxus - Dick Higgins - Intermedial Object #1

**Lù:** "There exist different worlds that may not intersect with each other - between fine arts, popular arts, industrial arts, and applied arts"

**Lù:** On "difficulties when adopting tools for artistic activities" -> information technology requires time and effort to acquire, to be fluent -> yet, are "new media" tools really "less free"? -> "new media" being categorised as "others" sounds like what "sisters with transistors", field that hasn't been "gate kept" by instituitions

**Lù:** "instituitionalised arts" -> the perception of "what is art" or how things "become art"


### Other thoughts
**Lù:** On the physicality of the digital interfaces >> One day, my partner asked me about how to type a phrase in Chinese. When demonstrating, Burak noticed that the exclamation mark in Chinese text takes up twice the space compared to the exclamation mark used with Latin alphabets. My (speculated) reason behind this difference in width is due to the design of metal alloy used in moveble type (printing) - as each character is bounded in equal square space, the punctuations may thus be designed with the same width for the consistency of printing and the ease of layouting. This phenomenon may exemplify the "physicality" of digital space. Though the user interface of a real-time messanger seems to be "immaterial," it still preserves the trace of the physical world - the influence from its predecessor with the limitation in the pre-digital time. (Reference: <https://www.thetype.com/2018/02/14211/>)

*And, and..*

**Lù:** My fundamental puzzle is, how are we look at these in a "non-eurocentric" manner?

**Jiayi:** Also curious about the above question.

**Jiayi:** 
**Objectives**
- Understand the methodology and implications of artefact analysis in extending human reach through design.
- Differentiate the roles and impacts of primary, secondary, and tertiary artefacts in various contexts.

**Key Learnings**
- **Artefacts as Extensions**: Human-made artefacts, like the decommissioned Arecibo Radiotelescope, extend our capabilities to interact with and comprehend the universe.
- **Conceptual Foundations**: Artefacts -> originate from the Latin 'artefactum' (**art - skill, facere - to make**), underscoring their intentional creation.
- **Primary, Secondary, and Tertiary Artefacts**: As per Marx Wartofsky's framework, primary artefacts directly alter environments, secondary artefacts represent these primary artefacts and extend their utility, and tertiary artefacts serve as imaginative constructs that reflect broader cognitive and affective needs.
- **Philosophical and Design Implications**: The distinction between natural and artifactual kinds suggests differing cognitive processes in their comprehension and use, which is crucial for designers to consider when envisioning artefacts in context.

**Insights and Reflections**
- Understanding the intended use and context of artefacts helps in appreciating their design and the foresight required to realize their potential.
- The philosophical discourse around artefacts challenges us to consider the deeper meanings and implications of the artefacts we daily interact with and rely on.

**Questions and Curiosities**
- How can designers effectively predict and design for the evolutionary use of artefacts in contexts that may not yet exist?
- In what ways do secondary and tertiary artefacts influence cultural transmission and societal norms?
- What are the ethical considerations in designing artefacts that significantly alter human capabilities or the environment?
- How might the concept of self-referentiality and authorship in artefacts impact the legal and moral rights associated with artefact creation and use?
- What is Speculative Design's relationship with artefact analysis？

**Possible Resources to Look Into Later**
- Explore **Katherine Hayles**’ discussions on the intersection of technology, literature, and culture to further understand the philosophical underpinnings of artefact analysis.
- **Stanford Encyclopedia of Philosophy**, for a comprehensive exploration of artefact theory and its implications in multiple fields.

**Engaging with Artefact Analysis**
- The detailed analysis of artefacts using frameworks like those presented by Risto Hilpinen and Marx Wartofsky provides a multi-layered understanding of how artefacts are constituted and the roles they play in human activity and perception. This analytical approach not only aids in designing more effective artefacts but also in interpreting their broader societal impacts.