---
title: "2024.01.15 - Joint Lecture 1: Introduction & Essentials"
date: 2024-01-15T09:39:28-07:00
---

## "Artistic research approaches and problematics" by Harri Laakso

**Lù:** Artistic research >> "in general" doesn't exit >> to be considered as a research activity, it is necessary to include (explicit / implicit) declaration of one's stance on art >> regarding that *individual researcher* << view the knowledge body (*communal*) through a *personal* lens

**Lù:** topic of artisitic research >> always **the relationship between images and words** *[WHY WORDS?]* >> the tasks is **write** *alongside* the works >> to create a new **paradoxical body**

**Lù:** *[BUT WHAT IS RESEARCH, AFTER ALL]* genuine searching, an investigative practice, institutional *[WHAT DO WE MEAN BY THIS ALSO]* or as a relation to the world

**Lù:** artistic research >> knowledge production
- not only to create formal knowlege but also to inspire "unfinished thinking"

**Lù:** singular nature of artistic research and/or art practice: it cannot be repeated as such << *[a question from the lectuere: can anything (be repeated)?]*
- I do think it is also applicable to scientific research, yet, of course, scientists may research on the same topic, with same question, yet the research method, its design, its environment, still differs
- Something *special* about artistic research may be >> the relations between the researcher as an individual and art

**Lù:** fiction >> "Everything is anyways fiction" (Juha Varto, Artistic Research) >> "fiction" as 1). a construct, 2). a framing of the world, 3). something uncovered/shown/revealed
- the etymology of "fiction" >> fingere = to make (forge) and to feign
- a particular understanding about something in the form of an artistic/aesthetic work or action
- to question one's own expertise
- *[YET, I am probably not a fan of academic fictions]* though the concept sounds like a fun research approach, the outputs are sometimes not as enjoyable to read as literary works (from an ordinary reader's perspective). Maybe it is too much the "will" from the author?

**Lù:** fiction >> language >> fantasy >> desire
- *excursion* >> phantasmic teaching >> "an age at which we teach what we do not know; this is *research*" >> future, unlearning, sapientia, wisdom
- can the teaching and research instituitions catch up with what's going on *outside the instituitions*

**Jonna:** Art as research practice. Experimental research can lead to unexpected directions. 

**Jonna:** It might be easier to define the desired output of scientific research. With artistic research, the experimental nature of the process makes it hard to define the direction from the beginning.

**Jonna:** Artistic research aims to produce knowledge. What is knowledge worthy enough for a thesis?

*[ON THE CLASSROOM DISCUSSION]*
**Lù:** It seems we form a dichotomy of "artistic versus scientific" in the discussion while we probably don't know what the "scientific research" actually is. About "imagination", it also exists in the knowledge production in sciences. For example, the mathematical imagination ([Mathematical imagination and embodied cognition](https://www.mathmoves.org/sites/default/files/nemirovsky_ferrara_2008_mathematical_imagination_0.pdf)). Can we learn "pigeonhole principle" without forming a mental image? Probably not. I genuinely wish that (we) artistic researchers and practitioners can be more curious and courageous to investigate what is going on out there.

**Jonna:** I agree, it seems a bit strange to separate artistic and scientific research or talk about them as if they were opposite. In the end, even when not collaborating, both types or research can find inspiration from each other.

**Lù:** Also, regarding "imagination" - I feel there exist multiple modes of imagination. For one mode, we form a mental model of how things (should) work based on our lived experience. It sometimes inspires us to develop our thoughts further, yet can also limit our understanding of a certain phenomenon.

## "Doing research in art and media" by Masood Masoodian

**Lù:** The purpose of research >> **to ask a significant** & **to find an answer to that question** (Leedy & Ormrod)

**Lù:** The research timeline >> Note that it does not proceed strictly as this gantt chart implies

**Jonna:** Artistic research process is often not linear.

**Jonna:** An answer to research question should require more than "yes" or "no".

**Jonna:** When are research questions useful? I've been told you don't necessarily need one in artistic research. Is a clear researh topic enough? Questions seem to help with direction.

**Jonna:** Research approach: qualitative, quantitative, applied or visual.