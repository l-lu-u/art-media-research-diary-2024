---
title: "2024.02.05 - Joint Lecture 4: Copyright for artists"
date: 2024-02-05T10:39:28-07:00
---

## Lecture: Copyright for artists

**Jonna:**
The copyright takes place once the work is ready enough to be an independent piece of work. Sometimes a sketch can also get copyright.
- Copyright for artwork: 70 years after the death of the artist.
- Copyright for photos (non-art): 50 years after the photo was taken.

- The artist always has a right to see and photgraph their own art. 

- Public art can be photographed for non-commercial use without permit. If the work is the main visual of a commercial piece, a permit is needed. 

- Copyright allows for parody. 

- Always make a contract when giving someone else the right to use your work!!

**Lù:** Ah... this is really helpful, and I wish we can learn more about how to negotiate and propose contract.