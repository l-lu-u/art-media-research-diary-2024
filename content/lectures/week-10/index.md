---
title: "2024.03.18 - New Media Session 5: Cultural Probes"
date: 2024-03-18T09:09:28-07:00
---

*Primary & Secondary Sources & Hybrid such as cultural probes*

***

## Lecture

**Lù:** Journals
- Leonardo
- Digital Creatives

**Lù:** Sage Research Methods
- Cultural Probes -> can this be used in my thesis?

**Lù:** Lily's questionaire about "What does 'multiculturalism' mean in the Finnish context?"

**Lù:** "Sentiment Analysis" -> How can LLM be applied to aid content analysis?

**Lù:** Kripendoff p.23 -> "Redesign Design discourse"
- "Colonialism of Design"

**Jiayi:**

**Objectives**
- To understand the classification and use of primary and secondary sources in research.
- To explore various research methods including qualitative and quantitative approaches, i.e. Cultural Probe in Qualitative Contextual Design Research.

**Key Learnings**
- **Primary Sources**: Unique to the researcher's work and provide direct, firsthand evidence about an event, object, person, or work of art.
- **Secondary Sources**: Derive their content from primary sources and provide interpretation, analysis, or a synthesis of data.
- **Research Methods**: Includes both qualitative methods like focus groups, case studies, and interviews, and quantitative approaches.

**Insights and Reflections**
- Working with sources is akin to detective work, requiring the researcher to gather, analyze, and synthesize information to build a solid foundation for their arguments.
- The role of cultural probes and other qualitative methods in research highlights the importance of context and depth in understanding phenomena.
- The balance between qualitative and quantitative research methods is crucial, depending on the nature of the inquiry and the specifics of the data needed.

**Questions and Curiosities**
- Is Cultural Probes really useful for the art creation process? Is it more for design instead of for art?
- How can one effectively integrate findings from both primary and secondary sources to present a balanced perspective in research?
- What are the practical challenges of conducting research using cultural probes, especially in terms of relevance and impact on the art creation process?
- How does the use of dialogical methods in presenting research enhance the understanding of the subject matter?

**Possible Resources to Look Into Later**
- **Sage Research Methods Tutorials**: For detailed guides on various research techniques.
- **Encyclopedias & Compendia** such as Wikipedia, Zlibrary
- **Peer-Reviewed Journals and Conference Proceedings**
- **Online Repositories and Audio Visual Sources**