---
title: "2024.01.29 - Joint Lecture 3: Doctorial Students' Presentation"
date: 2024-01-29T10:39:28-07:00
---

## Lecture: Doctorial Students' Presentation

**Jonna:**
Nicola Guido Cerioli talked about rhizomatic thinking. Could have been useful for my BA research about learning through games. 

I don't want to do data visualization but I should explore having data as inspiration/basis for games. ...so maybe I want to do data visualization just not business driven.

Greogoire Rousseau's book has nice fonts, I should find them later:
**<https://rousseau.fi/pdf/Electric_energy.pdf>**

Also, electricity as a form of energy (instead of body) in the artwork is an interesting approach. Electricity could create the artwork, not only enable it.

Can't believe I didn't know about Monoskope before... this helps so much. **<https://monoskop.org/Monoskop>**

**Lù**: It feels exciting to see how people pursueing research in art and media. This gives some concrete examples of what is (usually) expected and how can we approach one thing we are interested in.

### "Thesis does not define you."
