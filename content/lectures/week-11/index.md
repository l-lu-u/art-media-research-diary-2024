---
title: "2024.03.25 - New Media Session 6: Scenario Design"
date: 2024-04-10T09:09:28-07:00
---

## Lecture: Scenario Design

**Lù:** Boundary Object (e.g., prototypes, storyboards)

**Lù:** PoV -> in Kripendoff book (on making video):
- imagine sequence
- observe sequence

**Jiayi:**

**Objectives**
- Understand the relationship between the designer's model, the user's model, and the system image.
- Explore the concept of Scenario-Based Design from multiple perspectives.

**Key Learnings**
- **Conceptual Models**: They are the blueprints that define the interaction between the designer, user, and system.
- **Scenario-Based Design**: This method involves the use context from various angles, imagining potential situations and their concrete elements, often before they even exist.
- **Vannevar Bush's Influence**: His seminal work, "As We May Think," a theoretical framework for contemporary design scenarios.

**Insights and Reflections**
- Design is not just about building systems but about foreseeing user interactions and their possible outcomes. It's a proactive approach to create solutions that feel intuitive and meet unforeseen needs.
- Thinking from multiple perspectives not only enhances creativity but also prepares the design to handle more diverse user expectations.

**Questions and Curiosities**
- How can these conceptual models be adjusted dynamically as more real-world user data becomes available?
- What are some of the most successful practical applications of Scenario-Based Design in modern technology?

**Possible Resources to Look Into Later**
- Further explore Vannevar Bush's writings to understand the historical evolution of design thinking.
- Review case studies where Scenario-Based Design was pivotal in product development.

**Five Angles of Scenario-Based Design**
- A neat framework to dissect and analyze the design process, ensuring all bases are covered before a product hits the market.

***

## Peer review discussion

**Ville gave feedback on Lù's preliminary thesis plan**

**Lù:** Key points from Ville's review
- "Is the focus on pedagogy study or human machine interaction?" >> pedagogy
- (+) the idea of more-than-huamn as creative entities in the maker space, how machines' characteristics influence (limit or inspire) human agents' practices.