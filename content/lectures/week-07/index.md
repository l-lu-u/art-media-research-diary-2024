---
title: "2024.02.26 - New Media Session 2: Participatory Design"
date: 2024-02-26T10:39:28-07:00
---

## Lecture

**Lù:** One problem(?) I notice is that researchers sometimes project too much their own (value heavy) intepretations onto their observations of the communities or participants. I think this should also be addressed when conducting participatory research or design. If we aim to share the power, agency, or project ownership with the community, we should also acknowledge and respect the participants' own narractives when interpretating the gathered data.

**Jo:** For example, "making" from the Aalto's academic perspective can be very different to those like my grandpa that builds and repairs things every day. What is the necessity of urban people to define maker culture, are FabLabs really that revolutionary or just like any other community workshop (with a bigger price tag)?

**Jiayi:** 
**Objectives**
- The concept of 'raw matter' in new media + how it informs participatory design practices.
- The philosophical and practical aspects of ontology in the context of new media.

**Key Learnings**
- **Participatory Design**: Multiple levels of user engagement, including human-centered design (design *for* users), co-design (design *with* users), and user-generated design (design *by* users).
- **Ontology in New Media**: Determining what kinds of things can exist in the realm of new media + how these entities are categorized and interacted with.
- **'Raw Matter' of New Media**: The fundamental components used in new media designs: *data*, *code*, *user interfaces*(why?), which enable interactive and participatory experiences.

**Insights and Reflections**
- The shift from traditional media to new media brings 'raw matter' into *play*, expanding the designer’s toolkit with digital and interactive elements. Expand boundaries.
- Ontology helps frame these elements -> as tools, + -> as parts of the systems, influencing both the design process and the user experience.
- *The process of participatory design* emphasizes the community in shaping both the artefact and its use, involving users early and often in the design process.

**Questions and Curiosities**
- How can designers effectively add more user input throughout the design process without compromising designers' artistic vision.
- Ethical considerations when users help create digital artefacts that may store or transmit personal data.

**Possible Resources to Look Into Later**
- Further explore "The Four-Category Ontology: A Metaphysical Foundation for Natural Science" by E.J. Lowe to deepen understanding of ontological categories.
- Review examples of successful participatory design projects to identify best practices and common challenges.

**Engaging with the Material**
- Plan to attend or review recordings of co-design workshops (check **Eric Zimmerman**).