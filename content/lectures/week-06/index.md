---
title: "2024.02.12 - New Media Session 1: Introduction to the area of inquiry"
date: 2024-02-12T10:39:28-07:00
---

## Lecture: Introduction to the area of inquiry

**Jiayi:**
**Objectives**
- Components of new media research: concept, interaction, content, audience, interface...
- Distinctions between basic and applied research in art and design.

**Key Learnings**
- **Research Types**: **Basic research** builds the core foundation of knowledge; **Applied research** focuses on creating new artefacts through a practice-led approach.
- **Concept**: An abstract idea which can be as intangible as an emotion (e.g., joy) or a generalization like the use of the notion of 'tree' in complex data structures.
- **Interactivity**: Examines navigation from both egocentric (self-motivated) and allocentric (environment-cued) perspectives, using examples from virtual reality.
- **Content**: Defined as narrative elements that remain constant across different sign systems, illustrated by examples like Brian House’s "Quotidian Record".
- **Interface**: Describes various types of user interfaces: traditional GUIs, tangible user interfaces (TUI), and gesture-based interfaces, highlighting how they facilitate user interaction.

**Insights and Reflections**
- The concept of interactivity in new media is crucial as it defines how users engage with content, whether through direct interaction or influenced by environmental cues.
- Content in new media transcends traditional text, incorporating visual and sonic elements that enhance narrative storytelling.
- Interfaces are evolving to be more natural and intuitive -> integrating technology more into daily life.

**Questions and Curiosities**
- How can new media researchers balance (or how to choose the reseach focus between) the innovative demands of applied research with the theoretical depth required in basic research?
- What are the potential ethical considerations in designing interfaces that collect user data through natural interactions?
- How might the concept of audience demographics and community engagement change as new media platforms evolve?

**Possible Resources to Look Into Later**
- Further exploration into the use of conceptual maps like **Venn diagrams** and causal sequences to better organize and present research data.
- "Re-discovering Vrouw Maria" project.

***

**Jonna:**
**Thoughs..**
Research in art and media can be both basic or applied research. Can one thesis be both?

How does research differ from research led design project? When does something become academic research? What is independent academic research? Will people respect research when it's not connected to an institution?

Where is the line between personal project and autoethnographic research? Connecting to existing research? Context?

New words I learned: allocentric, perambulation

Computer mediaded perspective. Human computer interaction is changing, computers could be concidered equals to humen, but what does this mean? Do we want the machines to be equal? ...it's important that media researchers and artists are involved in the discourse, so that a wider part of society can direct the development of technology.

## Reading discussion

The reading discussed is:
> Thomas , L. M., & Glowacki, D. (2018). Seeing and feeling in VR: bodily perception in the gaps between layered realities. *International Journal of Performance Arts and Digital Media.* https://doi.org/10.1080/14794713.2018.1499387

**Lù:** In the text, there are several words being used for the opposite to "virtual" >>
- material
- physicial
- mass
- tangible
- real (?)
...

From this list of adjectives, we may realise that there are multiple dimensions of the discussion about the experience in VR in the text. Fundamentally, I think, it is a discussion about how to perceive the world. While wearing the headset, we are still "seeing", yet the visual elements in fact don't have mass - but, through constructing the physical and the haptic feedback, the virtual reality can be perceived as convincing and intuitive as the world we have been living in.

When I commented about the "rules" in VR environment, Vytautas extended the dicussion to the social rules in the VR world. I agree that it also greatly affects how we navigate through the environment, yet, I think this is on a slightly different layer - the design of "gameplay" (instead of some more infrastructure level design).

**Jonna:**
Should VR be following the same rules as IRL? Is it ethical to create something that is not acceptable in the real world? The current form of VR is often concidered "safe" by default. What happens if VR becomes more immersive or body integrated? Is there going back is ethics are not discussed more today?