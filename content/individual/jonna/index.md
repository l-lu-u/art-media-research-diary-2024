---
title: "Jonna (ELORANTA)"
date: 2024-03-26T09:39:28-07:00
---

## hELLO

I'm Jonna. It's been fun to document some of our conversations after the lectures. Here's my personal website: <https://jonnaelo.github.io/>


Thinking about my thesis...inspired by Gregoire Rousseau:
- What if I made my whole thesis using open source? Could I actually finish?
- I'm interested in accessible academics, but how to practice these ideas inside an institution like Aalto? 
- Being forced to use Aalto's services makes it impossible to do a thesis that only uses open source materials, services etc. 
- Can all research be public one day?
