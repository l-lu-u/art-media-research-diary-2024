---
title: "Lù (CHEN)"
date: 2024-04-24T09:39:28-07:00
---

👋 你好！ Moi! Hej! Hello! I'm Lù. I set up this repository for me and my classmates Jonna and Jiayi to write our learning diaries for "Doing Research in Art and Media" together. Initially, Jonna and I realised that we often discussed the lecture content together, and the discussion actually enrich our learning in the course. Thus, we decided to do the diary in this collaborative manner.

When talking about the learning diary assignment, Lily suggested us to think about the "categories" for the diary and how the categorisation can benefit for our own practices. It makes me contemplate about journaling, in some way, this documentation can be the initial step of archiving my own practices. Also, more than just the records of what I have done and learnt, it notes down ideas for the future as well.

## Inspirations or resources about "Living Archive"
- The Museum of Unconditional Surrender (Dubravka Ugrešić)
  - <https://www.themodernnovel.org/europe/europe/croatia/dubravka-ugresic/the-museum-of-unconditional-surrender/>
  - <https://theofficeforcurating.com/wp-content/uploads/2014/05/The-Museum-of-Unconditional-Surrender-ENG.pdf>