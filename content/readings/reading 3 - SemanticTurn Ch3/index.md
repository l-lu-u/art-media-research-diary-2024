---
title: "Reading 3: ‘The Semantic Turn’ Chapter 3. 'Meaning of artifacts in use'"
date: 2024-03-25T09:39:28-07:00
---

Krippendorff, K. (2006). *The Semantic Turn.* CRC/Taylor & Francis.
Ch. 3, “Meaning of artifacts in use”, in The Semantic Turn, pp. 77-145 Exercise

## General
