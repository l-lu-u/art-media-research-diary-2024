---
title: "Reading 2: ‘The Semantic Turn’ Chapter 1. 'History and Aim' & Chapter 2. 'Basic concepts in human-centred design'"
date: 2024-03-11T09:39:28-07:00
---

Krippendorff, K. (2006). *The Semantic Turn.* CRC/Taylor & Francis.
Ch. 1 “History and Aim” and Ch. 2, “Basic concepts in human-centred design”, pp. 1- 70.

## General
