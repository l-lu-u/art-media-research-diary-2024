---
title: "Reading 4: ‘The Semantic Turn’ Chapter 4. 'Meaning of artifacts in language'"
date: 2024-04-08T09:39:28-07:00
---

Krippendorff, K. (2006). *The Semantic Turn.* CRC/Taylor & Francis.
Ch. 4, “Meaning of artifacts in language”, in The Semantic Turn, pp. 147-176.

## General
