---
title: "Reading 1: ‘Seeing and feeling in VR: bodily perception in the gaps between layered realities’"
date: 2024-01-08T09:39:28-07:00
---

Thomas, L.M., Glowacki, D.R. (2018). ”Seeing and feeling in VR: bodily perception in the gaps
between layered realities”, *International Journal of Performance Arts and Digital Media,* 14:2, 145-168,
link, <https://www.tandfonline.com/doi/full/10.1080/14794713.2018.1499387>

## General
